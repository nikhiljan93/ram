package com.excelsior.ram;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class AddUserActivity extends Activity {

    File userFolder;
    File userData;

    File profileData;

    int profileID;

    EditText pr_email;
    EditText pr_number;
    EditText pr_dob;
    EditText pr_notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent intent = getIntent();
        userFolder = (File) intent.getSerializableExtra("user");
        userData = new File(userFolder, getString(R.string.file_emergency));

        profileID = intent.getIntExtra("id", 0);

        TextView tv = (TextView) findViewById(R.id.code_view);
        tv.setText("" + profileID);

        pr_email = (EditText) findViewById(R.id.profile_email);
        pr_number = (EditText) findViewById(R.id.profile_number);
        pr_dob = (EditText) findViewById(R.id.profile_dob);
        pr_notes = (EditText) findViewById(R.id.profile_notes);

        final Button button_save = (Button) findViewById(R.id.profile_save);
        button_save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveProfile();
                button_save.setEnabled(false);
            }
        });

        final Button button_add_contact = (Button) findViewById(R.id.button_add_new);
        button_add_contact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addContact();
            }
        });

        final Button button_del_contact = (Button) findViewById(R.id.button_del);
        button_del_contact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                delContact();
            }
        });

        openProfile();

        pr_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                button_save.setEnabled(true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        pr_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                button_save.setEnabled(true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        pr_dob.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                button_save.setEnabled(true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        pr_notes.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                button_save.setEnabled(true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        final SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putBoolean("emergency", true);
        editor.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveProfile() {

        try {

            profileData = new File(userFolder, getString(R.string.file_profile));

            BufferedWriter output = new BufferedWriter(new FileWriter(profileData));
            output.write(pr_email.getText().toString() + "\n");
            output.write(pr_number.getText().toString() + "\n");
            output.write(pr_dob.getText().toString() + "\n");
            output.write(pr_notes.getText().toString() + "\n");
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openProfile() {

        try {

            profileData = new File(userFolder, getString(R.string.file_profile));

            if (profileData.exists()) {

                BufferedReader br = new BufferedReader(new FileReader(profileData));
                pr_email.setText(br.readLine());
                pr_number.setText(br.readLine());
                pr_dob.setText(br.readLine());
                pr_notes.setText(br.readLine());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addContact() {

        EditText temp = (EditText) findViewById(R.id.contact_name);
        String temp_str = temp.getText().toString();

        if (!checkExists(temp_str)) {
            try {

                BufferedWriter output = new BufferedWriter(new FileWriter(userData, true));

                output.write(temp_str + "\n");

                temp = (EditText) findViewById(R.id.contact_id);
                temp_str = temp.getText().toString();
                output.write(temp_str + "\n");

                temp = (EditText) findViewById(R.id.contact_no);
                temp_str = temp.getText().toString();
                output.write(temp_str + "\n");

                output.close();
                Toast.makeText(this, "Contact added", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void delContact() {

        boolean found = false;

        BufferedReader br = null;
        BufferedWriter br2 = null;

        File tempFile = new File(userFolder, "temp.ram");

        EditText temp = (EditText) findViewById(R.id.contact_name);
        String temp_str = temp.getText().toString();

        try {

            String sCurrentLine;
            int cntr = 3;

            br = new BufferedReader(new FileReader(userData));
            br2 = new BufferedWriter(new FileWriter(tempFile));

            while ((sCurrentLine = br.readLine()) != null) {
                if (sCurrentLine.equalsIgnoreCase(temp_str) || (cntr < 3)) {
                    found = true;
                    cntr--;
                    if (cntr == 0)
                        cntr = 3;
                    continue;
                } else {
                    br2.write(sCurrentLine + "\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                br2.close();
                userData.delete();
                tempFile.renameTo(userData);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        if (found)
            Toast.makeText(this, "Contact deleted", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Contact not found", Toast.LENGTH_SHORT).show();

    }

    private boolean checkExists(String name) {

        BufferedReader br = null;

        if (!userData.exists()) {
            return false;
        }

        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader(userData));
            while ((sCurrentLine = br.readLine()) != null) {
                if (sCurrentLine.equalsIgnoreCase(name)) {
                    Toast.makeText(this, "Contact already exists", Toast.LENGTH_SHORT).show();
                    br.close();
                    return true;
                } else {
                    for (int x = 1; x <= 2; x++)
                        br.readLine();
                }
            }

            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
