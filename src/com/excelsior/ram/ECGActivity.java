package com.excelsior.ram;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphView.LegendAlign;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ECGActivity extends Activity {

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    // Intent request codes
    private static final int REQUEST_ENABLE_BT = 2;
    //GraphView_ecg init
    static LinearLayout GraphView_ecg;
    static GraphView graphView_ecg;
    static GraphViewSeries Series_ecg;
    //GraphView_beat init
    static LinearLayout GraphView_beat;
    static GraphView graphView_beat;
    static GraphViewSeries Series_beat;
    //graph value
    private static double graph2LastXValue_ecg = 0;
    //graph value
    private static double graph2LastXValue_beat = 0;
    private static int Xview = 10;
    final Handler handler = new Handler();
    Button button_slow = null;
    Button button_fast = null;
    Button button_norm = null;
    Button button_real = null;
    Button button_stop = null;
    File sessionAccess = null;
    File replayFile_ecg = null;
    File replayFile_beat = null;
    File emergency;
    BufferedWriter writeLog_ecg = null;
    BufferedWriter writeLog_beat = null;
    double averagingArray[] = new double[10];
    double peaks_r_average = 0.0;
    double peaks_r_average_prev = 0.0;
    String timeNow;
    TextView bpm = null;
    TextView bpm_avg = null;
    TextView analysis = null;
    EmergencyService officer;
    int timeElapsed = 1;
    int index = 0;
    boolean reported = false;
    int index_analyse = 0;
    int filtered;
    double scale = 1.5;
    double ampScale = 1.0;
    double peaks_limit = 1.0;
    int ampLimit = 60;

    boolean emergency_state = true;

    private Runnable runnable_analyse = new Runnable() {
        @Override
        public void run() {

            if ((peaks_r_average * 60 * scale) > 150) {
                index_analyse++;
                Log.d("Weight value high", index_analyse + "");
            } else if ((peaks_r_average * 60 * scale) < 50) {
                index_analyse--;
                Log.d("Weight value low", index_analyse + "");
            } else {
                if ((peaks_r_average_prev * 60 * scale) > 150) {
                    index_analyse -= 2;
                    Log.d("Weight value high prev", index_analyse + "");
                } else if ((peaks_r_average_prev * 60 * scale) < 50) {
                    index_analyse += 2;
                    Log.d("Weight value low prev", index_analyse + "");
                } else {
                    index_analyse = 0;
                    Log.d("Weight value normal prev", index_analyse + "");
                }
            }
            peaks_r_average_prev = peaks_r_average;

            try {

                if (index_analyse > 7) {
                    analysis.setText("Tachycardia imminent!");
                    analysis.setTextColor(Color.RED);
                    if (!reported) {
                        writeLog_ecg.flush();
                        if (emergency_state)
                            officer.dispatchRequest("Tachycardia Alert! Last known heart rate: " + (int) (peaks_r_average * 60 * scale), replayFile_ecg);
                        reported = true;
                    }
                } else if (index_analyse < -7) {
                    analysis.setText("Bradycardia imminent!");
                    analysis.setTextColor(Color.RED);
                    if (!reported) {
                        writeLog_ecg.flush();
                        if (emergency_state)
                            officer.dispatchRequest("Bradycardia Alert! Last known heart rate: " + (int) (peaks_r_average * 60 * scale), replayFile_ecg);
                        reported = true;
                    }
                } else {
                    analysis.setText("Normal");
                    analysis.setTextColor(Color.BLUE);
                    reported = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            handler_analyse.postDelayed(this, 3000);
        }
    };
    double peaks_r = 0.0;
    double peaks_r_actual;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            peaks_r_actual = peaks_r / (timeElapsed);
            if (peaks_r_actual > 2) {

                if (peaks_r_actual > 5) {

                    peaks_r_actual = 3;
                } else {
                    peaks_r_actual = peaks_r_actual / peaks_limit;
                }

            }
            peaks_r_average = averagingFilter(peaks_r_actual);
            if (peaks_r_average == 0) {
                index_analyse = -8;
            }
            Log.d("come on", "r" + peaks_r);
            Log.d("come on", "r_actual " + peaks_r_actual);
            peaks_r = 0;
            timeElapsed++;

            handler_time.postDelayed(this, 1000);
        }
    };
    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {

        String finalMessage = "";
        String actualMessage = "";

        double peaks_processed;

        @SuppressWarnings("deprecation")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            //mTitle.setText(R.string.title_connected_to);
                            //mTitle.append(mConnectedDeviceName);
                            finalMessage = "";
                            actualMessage = "";
                            Log.d("BlueDisc", "Flushing Buffers");
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            // mTitle.setText(R.string.title_connecting);
                            finalMessage = "";
                            actualMessage = "";
                            Log.d("BlueDisc", "Flushing Buffers");
                            break;
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            //mTitle.setText(R.string.title_not_connected);
                            finalMessage = "";
                            actualMessage = "";
                            Log.d("BlueDisc", "Flushing Buffers");
                            handler_time.removeCallbacks(runnable);
                            handler_analyse.removeCallbacks(runnable_analyse);
                            break;
                    }
                    break;
                case MESSAGE_READ:
                    String readMessage = (String) msg.obj;
                    if (msg.arg1 > 0) {

                        filtered = (int) readMessage.charAt(0);
                        filtered = (int) (filtered * ampScale);

                        peaks_processed = (peaks_r_actual * 60 * scale);

                        if (filtered > 50 && filtered < ampLimit) {
                            timeElapsed = 1;
                            Log.d("Peaked!", "" + filtered);
                            peaks_r++;//
                        }
                        bpm.setText((int) peaks_processed + "");
                        bpm_avg.setText((int) (peaks_r_average * 60 * scale) + "");

                        try {

                            Series_ecg.appendData(new GraphViewData(graph2LastXValue_ecg, filtered), true, 200);
                            Series_beat.appendData(new GraphViewData(graph2LastXValue_beat, (int) (peaks_r_average * 60 * scale)), true, 500);

                            if (sessionAccess != null) {
                                writeLog_ecg.write(filtered + "\n");
                                writeLog_beat.write((peaks_r_average * 60 * scale) + "\n");
                            }

                            //X-axis control
                            graph2LastXValue_ecg += 0.05;
                            graph2LastXValue_beat += 0.05;

                            graphView_ecg.setViewPort(graph2LastXValue_ecg - Xview, Xview);
                            graphView_beat.setViewPort(graph2LastXValue_beat - Xview, Xview);

                            //refresh
                            GraphView_ecg.removeView(graphView_ecg);
                            GraphView_ecg.addView(graphView_ecg);
                            GraphView_beat.removeView(graphView_beat);
                            GraphView_beat.addView(graphView_beat);

                            //sendBTMessage("a");

                        } catch (Exception e) {
                            Log.d("What!?", "What's happening?");
                            Log.d("DebugActualMessage", "Value: " + actualMessage);
                            Log.d("DebugFinalMessage", "Value: " + finalMessage);
                            sendBTMessage("t");
                        }
                    }
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    Toast.makeText(getApplicationContext(), "Connected to HealthBot(TM)", Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        /*public boolean isFloatNumber(String num) {
            //Log.d("checkfloatNum", num);
            try {
                Double.parseDouble(num);
            } catch (NumberFormatException nfe) {
                return false;
            }
            return true;
        }*/
    };
    private CheckBox enable_sim;
    private CharSequence mTitle;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Name of the connected device
    //private String mConnectedDeviceName = null;
    // Member object for the chat services
    private BluetoothChatService mChatService = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    private Handler handler_time = new Handler();
    private Handler handler_analyse = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("ECG Monitoring");
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);//Hide title
        //this.getWindow().setFlags(WindowManager.LayoutParams.
        //FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);//Hide Status bar

        setContentView(R.layout.activity_ecg_alt);

        init_ecg();
        init_beat();

        sessionAccess = initLinkUser();

        if (sessionAccess == null) {
            Toast.makeText(this, "No user logged in", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Auto-logging has begun", Toast.LENGTH_SHORT).show();
        }

        button_slow = (Button) findViewById(R.id.ecg_slow);
        button_slow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendBTMessage("s");
                scale = 0.75;
            }
        });

        button_fast = (Button) findViewById(R.id.ecg_fast);
        button_fast.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendBTMessage("f");
                scale = 2;
            }
        });

        button_norm = (Button) findViewById(R.id.ecg_normal);
        button_norm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendBTMessage("n");
                scale = 1.5;
            }
        });

        button_real = (Button) findViewById(R.id.ecg_real);
        button_real.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendBTMessage("t");
                sendBTMessage("r");
                scale = 1.0;
                ampScale = 1.0;
                ampLimit = 60;
                peaks_limit = 1.6;
                button_stop.setEnabled(true);
                button_real.setEnabled(false);
                enable_sim.setEnabled(false);
                handler_time.postDelayed(runnable, 1000);
                handler_analyse.postDelayed(runnable_analyse, 6000);
            }
        });

        button_stop = (Button) findViewById(R.id.ecg_stop);
        button_stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendBTMessage("t");
                scale = 1.0;
                ampScale = 1.0;
                peaks_limit = 1.0;
                ampLimit = 60;
                button_stop.setEnabled(false);
                button_real.setEnabled(true);
                enable_sim.setEnabled(true);
                handler_time.removeCallbacks(runnable);
                handler_analyse.removeCallbacks(runnable_analyse);
            }
        });

        enable_sim = (CheckBox) findViewById(R.id.ecg_enable_sim);
        enable_sim.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (((CheckBox) v).isChecked()) {
                    button_slow.setEnabled(true);
                    button_norm.setEnabled(true);
                    button_fast.setEnabled(true);
                    button_real.setEnabled(false);
                    button_stop.setEnabled(false);
                    sendBTMessage("t");
                    sendBTMessage("a");
                    scale = 1.0;
                    ampScale = 1.0;
                    peaks_limit = 1.4;
                    ampLimit = 70;
                    handler_time.postDelayed(runnable, 1000);
                    handler_analyse.postDelayed(runnable_analyse, 6000);
                } else {
                    button_slow.setEnabled(false);
                    button_norm.setEnabled(false);
                    button_fast.setEnabled(false);
                    button_real.setEnabled(true);
                    sendBTMessage("t");
                    //sendBTMessage("r");
                    scale = 1.0;
                    ampScale = 1.0;
                    peaks_limit = 1.6;
                    ampLimit = 60;
                    handler_time.removeCallbacks(runnable);
                    handler_analyse.removeCallbacks(runnable_analyse);
                }
            }
        });

        bpm = (TextView) findViewById(R.id.ecg_bpm);
        bpm_avg = (TextView) findViewById(R.id.ecg_bpm_avg);
        analysis = (TextView) findViewById(R.id.ecg_analysis);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        officer = new EmergencyService(true, true, emergency);
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        emergency_state = prefs.getBoolean("emergency", true);
    }

    private double averagingFilter(double newData) {

        double tempAverage = 0.0;

        averagingArray[index] = newData;
        for (int p = 0; p < 10; p++) {
            tempAverage = (tempAverage) + averagingArray[p];
        }

        index++;
        if (index > 9) {
            index = 0;
        }
        return (tempAverage / 10);
    }

    private void init_ecg() {

        //init graphview
        GraphView_ecg = (LinearLayout) findViewById(R.id.Graph_ecg);
        // init example series data-------------------
        Series_ecg = new GraphViewSeries("",
                new GraphViewSeriesStyle(Color.rgb(40, 122, 169), 10),//color and thickness of the line
                new GraphViewData[]{new GraphViewData(0, 0)});
        graphView_ecg = new LineGraphView(
                this // context
                , "" // heading
        );
        graphView_ecg.setViewPort(0, Xview);
        graphView_ecg.setScrollable(true);
        graphView_ecg.setScalable(false);
        graphView_ecg.setShowLegend(false);
        graphView_ecg.setLegendAlign(LegendAlign.BOTTOM);
        graphView_ecg.setManualYAxis(true);
        graphView_ecg.setManualYAxisBounds(100, 0);
        graphView_ecg.addSeries(Series_ecg); // data
        GraphView_ecg.addView(graphView_ecg);
        graphView_ecg.getGraphViewStyle().setHorizontalLabelsColor(Color.TRANSPARENT);
        graphView_ecg.getGraphViewStyle().setVerticalLabelsColor(Color.TRANSPARENT);
        graphView_ecg.getGraphViewStyle().setGridColor(Color.BLACK);
        //graphView_ecg.setDrawingCacheEnabled(true);
        graphView_ecg.setClipToPadding(true);
    }

    private void init_beat() {

        //init graphview
        GraphView_beat = (LinearLayout) findViewById(R.id.Graph_beat);
        // init example series data-------------------
        Series_beat = new GraphViewSeries("",
                new GraphViewSeriesStyle(Color.rgb(255, 0, 0), 10),//color and thickness of the line
                new GraphViewData[]{new GraphViewData(0, 0)});
        graphView_beat = new LineGraphView(
                this // context
                , "" // heading
        );
        graphView_beat.setViewPort(0, Xview);
        graphView_beat.setScrollable(true);
        graphView_beat.setScalable(false);
        graphView_beat.setShowLegend(false);
        graphView_beat.setLegendAlign(LegendAlign.BOTTOM);
        graphView_beat.setManualYAxis(true);
        graphView_beat.setManualYAxisBounds(300, 0);
        graphView_beat.addSeries(Series_beat); // data
        GraphView_beat.addView(graphView_beat);
        graphView_beat.getGraphViewStyle().setHorizontalLabelsColor(Color.TRANSPARENT);
        graphView_beat.getGraphViewStyle().setVerticalLabelsColor(Color.TRANSPARENT);
        graphView_beat.getGraphViewStyle().setGridColor(Color.BLACK);
        //graphView_beat.setDrawingCacheEnabled(true);
        graphView_ecg.setClipToPadding(true);
    }

    private File initLinkUser() {

        BufferedReader br;
        String temp;

        File session = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/open");

        try {
            if (session.exists()) {
                br = new BufferedReader(new FileReader(session));
                temp = br.readLine();
                emergency = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + temp + "/emergency_details.ram");
                session = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + temp + "/Heart/");

                if (!session.exists()) {

                    session.mkdirs();
                }

                timeNow = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());

                replayFile_ecg = new File(session, "ECG " + timeNow + ".replay");
                writeLog_ecg = new BufferedWriter(new FileWriter(replayFile_ecg, true));

                replayFile_beat = new File(session, "Beat " + timeNow + ".replay");
                writeLog_beat = new BufferedWriter(new FileWriter(replayFile_beat, true));

                return session;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ecg, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_storage:
                if (sessionAccess == null) {
                    Toast.makeText(this, "No user logged in", Toast.LENGTH_SHORT).show();
                } else {
                    Intent replay = new Intent(ECGActivity.this, ListReplayActivity.class);
                    replay.putExtra("userDir", sessionAccess);
                    replay.putExtra("heart", true);
                    handler_analyse.removeCallbacks(runnable_analyse);
                    startActivity(replay);
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mChatService == null) setupChat();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();


        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
    }

    @Override
    public synchronized void onDestroy() {
        super.onDestroy();

        sendBTMessage("t");

        handler_time.removeCallbacks(runnable);
        handler_analyse.removeCallbacks(runnable_analyse);

        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();

        if (sessionAccess != null) {
            try {
                Log.d("Writer", "Am I successful?");
                writeLog_ecg.write(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()) + "\n");
                writeLog_ecg.close();

                writeLog_beat.write(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()) + "\n");
                writeLog_beat.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setupChat() {

        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService = new BluetoothChatService(this, mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");

        // Get the device MAC address
        String address = getString(R.string.device_address);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device);
    }

    // Highpass FIR filter coefficients for 17 taps to filter < 2Hz
   /* static int coeffshp[] = {
        -763, -1267, -1091, -1867, -1969, -2507, -2619, -2911, 29908 };

    long mul16(int x, int y)
    {
        return (x*y);
    }

    private long beatFilter(int samplehp)
    {
        int bufhp[] = new int[32];
        int offsethp = 0;
        long z;
        int i;

        bufhp[offsethp] = samplehp;
        z = mul16(coeffshp[8], bufhp[(offsethp - 8) & 0x1F]);

        for (i = 0;  i < 8;  i++){
            z += mul16(coeffshp[i], bufhp[(offsethp - i) & 0x1F] + bufhp[(offsethp - 16 + i) & 0x1F]);
        }

        offsethp = (offsethp + 1) & 0x1F;
        return  z >> 15;                          // Return filter output
    }*/

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendBTMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);

            // Reset out string buffer to zero
            mOutStringBuffer.setLength(0);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Toast.makeText(this, "Error on enable Bluetooth", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

}
