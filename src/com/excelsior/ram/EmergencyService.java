package com.excelsior.ram;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by Alok on 4/27/2014.
 */
public class EmergencyService {

    boolean setMail;
    boolean setPhone;
    File contextFile;

    int nofContacts = 0;

    ArrayList<String> contactName = new ArrayList<String>();
    ArrayList<String> contactMail = new ArrayList<String>();
    ArrayList<String> contactPhone = new ArrayList<String>();

    public EmergencyService(boolean mail, boolean phone, File emergencyFile) {

        setMail = mail;
        setPhone = phone;
        contextFile = emergencyFile;

    }

    public void dispatchRequest(String Message_send, File attachLog) {

        getContactDetails();

        for (int th = 0; th < nofContacts; th++) {

            new SmsService().execute(contactPhone.get(th), Message_send);
            new SendMail().execute(Message_send, contactMail.get(th), attachLog.getAbsolutePath());
        }

    }

    public void getContactDetails() {

        String temp;

        try {
            BufferedReader br = new BufferedReader(new FileReader(contextFile));
            while ((temp = br.readLine()) != null) {
                contactName.add(temp);
                contactMail.add(br.readLine());
                contactPhone.add(br.readLine());
                nofContacts++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
