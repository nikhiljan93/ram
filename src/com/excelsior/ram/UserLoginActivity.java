package com.excelsior.ram;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class UserLoginActivity extends Activity {

    Button btn_add;
    Button btn_list;

    String profileID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Show the Up button in the action bar.
        setupActionBar();

        Intent intent = getIntent();

        profileID = intent.getStringExtra("id");

        TextView userName = (TextView) findViewById(R.id.control_name);
        userName.setText(getName());

        TextView userID = (TextView) findViewById(R.id.control_id);
        userID.setText(profileID);

        TextView email = (TextView) findViewById(R.id.control_email);
        email.setText(getMail());

        TextView contact = (TextView) findViewById(R.id.control_contact);
        contact.setText(getContact());

        TextView notes = (TextView) findViewById(R.id.control_notes);
        notes.setText(getNotes());

        btn_add = (Button) findViewById(R.id.control_add_new);

        btn_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openContact();
            }
        });

        btn_list = (Button) findViewById(R.id.control_list_existing);

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        boolean emergency_state = prefs.getBoolean("emergency", true);

        final SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();

        CheckBox repeatChkBx = (CheckBox) findViewById(R.id.check_emergency);
        repeatChkBx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editor.putBoolean("emergency", true);
                    editor.commit();
                    btn_add.setEnabled(true);
                    btn_list.setEnabled(true);
                } else {
                    editor.putBoolean("emergency", false);
                    editor.commit();
                    btn_add.setEnabled(false);
                    btn_list.setEnabled(false);
                }

            }
        });

        repeatChkBx.setChecked(emergency_state);
    }

    /**
     * Set up the {@link android.app.ActionBar}.
     */
    private void setupActionBar() {

        getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getName() {

        File temp = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + profileID);

        try {

            File temp1 = new File(temp, "profile.id");
            String nameValue;
            BufferedReader br = new BufferedReader(new FileReader(temp1));
            nameValue = br.readLine();
            br.close();

            return nameValue;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getMail() {

        File temp = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + profileID);

        try {

            File temp1 = new File(temp, getString(R.string.file_profile));
            String mailValue;
            BufferedReader br = new BufferedReader(new FileReader(temp1));
            mailValue = br.readLine();
            br.close();

            return mailValue;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getContact() {

        File temp = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + profileID);

        try {

            File temp1 = new File(temp, getString(R.string.file_profile));
            String contactValue;
            BufferedReader br = new BufferedReader(new FileReader(temp1));
            br.readLine();
            contactValue = br.readLine();
            br.close();
            return contactValue;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getNotes() {

        File temp = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + profileID);

        try {

            File temp1 = new File(temp, getString(R.string.file_profile));
            String notesValue = "";
            String tempVal;
            BufferedReader br = new BufferedReader(new FileReader(temp1));
            br.readLine();
            br.readLine();
            br.readLine();
            while ((tempVal = br.readLine()) != null) {
                notesValue = notesValue.concat(" " + tempVal);
            }
            br.close();
            return notesValue;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void openContact() {

        File userBase = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + profileID);
        Intent e = new Intent(UserLoginActivity.this, AddUserActivity.class);
        e.putExtra("user", userBase);
        e.putExtra("id", Integer.parseInt(profileID));
        startActivity(e);
    }
}
