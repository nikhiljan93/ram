package com.excelsior.ram;


import android.app.Activity;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ThermoActivity extends Activity {

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_DEVICE_NAME = 4;
    // Intent request codes
    private static final int REQUEST_ENABLE_BT = 2;
    //GraphView init
    static LinearLayout GraphView;
    static LineGraphView graphView;
    static GraphViewSeries Series;
    private static int Xview = 10;
    final Handler handler = new Handler();
    NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("Emergency!")
                    .setContentText("The body temperature just exceeded 35 C");
    File sessionAccess = null;
    File replayFile = null;
    BufferedWriter writeLog = null;
    String timeNow;
    // Sets an ID for the notification
    int mNotificationId = 001;

    // Key names received from the BluetoothChatService Handler
    // public static final String DEVICE_NAME = "device_name";
    File emergency;
    EmergencyService officer;
    String finalMessage = "";
    String actualMessage = "";
    int limitCount = 0;
    boolean emergency_state = true;
    private CharSequence mTitle;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Name of the connected device
    //private String mConnectedDeviceName = null;
    // Member object for the chat services
    private BluetoothChatService mChatService = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    //graph value
    private double graph2LastXValue = 0;
    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            //mTitle.setText(R.string.title_connected_to);
                            //mTitle.append(mConnectedDeviceName);
                            sendBTMessage("b");
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            // mTitle.setText(R.string.title_connecting);
                            break;
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            //mTitle.setText(R.string.title_not_connected);
                            finalMessage = "";
                            actualMessage = "";
                            Log.d("BlueDisc", "Flushing Buffers");
                            break;
                    }
                    break;
                case MESSAGE_READ:
                    String readMessage = (String) msg.obj;
                    if (msg.arg1 > 0) {
                        dataManager(readMessage);
                    }
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    //mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), "Connected to HealthBot(TM)"
                            , Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        private void dataManager(String incoming) {

            try {

                // Gets an instance of the NotificationManager service
                NotificationManager mNotifyMgr =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                TextView tView = (TextView) findViewById(R.id.curTemp);
                //SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ThermoActivity.this);
                //String emergencyNo = sharedPref.getString("emergency_enable_sms", "");
                finalMessage = finalMessage.concat(incoming);
                if (finalMessage.charAt(0) == 'a') {
                    if (finalMessage.length() >= 2) {
                        actualMessage = finalMessage.substring(1, finalMessage.indexOf('*'));
                        finalMessage = finalMessage.substring(finalMessage.indexOf('^') + 1, finalMessage.length());

                        if (isFloatNumber(actualMessage)) {

                            Series.appendData(new GraphViewData(graph2LastXValue, Double.parseDouble(actualMessage)), true, 100);

                            if (sessionAccess != null) {
                                writeLog.write(actualMessage + "\n");
                            }

                            //X-axis control
                            graph2LastXValue += 0.750;

                            graphView.setViewPort(graph2LastXValue - Xview, Xview);

                            //refresh
                            GraphView.removeView(graphView);
                            GraphView.addView(graphView);
                            tView.setText(actualMessage + " " + (char) 0x00B0 + "C");

                            if ((Double.parseDouble(actualMessage) >= 35)) {
                                graphView.setBackgroundColor(Color.argb(128, 255, 0, 0));
                                if (limitCount == 0) {
                                    mBuilder.setOngoing(true);
                                    mNotifyMgr.notify(mNotificationId, mBuilder.build());
                                    writeLog.flush();
                                    if (emergency_state)
                                        officer.dispatchRequest("Emergency! The patient's body temperature crossed " + actualMessage + ". The pertinent log has been attached.", replayFile);
                                }
                                limitCount++;
                                //Toast.makeText(getApplicationContext(), "That's too hot for comfort!",
                                //		Toast.LENGTH_SHORT).show();
                            } else if ((Double.parseDouble(actualMessage) < 35)) {
                                graphView.setBackgroundColor(Color.argb(128, 42, 122, 169));
                                mBuilder.setOngoing(false);
                                mNotifyMgr.cancel(mNotificationId);
                                limitCount = 0;
                            }
                            sendBTMessage("b");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("What!?", "What's happening?");
                Log.d("DebugActualMessage", "Value: " + actualMessage);
                Log.d("DebugFinalMessage", "Value: " + finalMessage);
            }
        }

        public boolean isFloatNumber(String num) {
            try {
                Double.parseDouble(num);
            } catch (NumberFormatException nfe) {
                return false;
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Thermometer");

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_thermo);

        initGraph();
        sessionAccess = initLinkUser();

        if (sessionAccess == null) {
            Toast.makeText(this, "No user logged in", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Auto-logging has begun", Toast.LENGTH_SHORT).show();
        }

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        officer = new EmergencyService(true, true, emergency);

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        emergency_state = prefs.getBoolean("emergency", true);
    }

    private void initGraph() {

        //init graphview
        GraphView = (LinearLayout) findViewById(R.id.Graph);
        // init example series data-------------------
        Series = new GraphViewSeries("",
                new GraphViewSeriesStyle(Color.TRANSPARENT, 10),//color and thickness of the line
                new GraphViewData[]{new GraphViewData(0, 0)});
        graphView = new LineGraphView(
                this // context
                , "" // heading
        );
        graphView.setBackgroundColor(Color.argb(128, 40, 122, 169));
        graphView.getGraphViewStyle().setNumVerticalLabels(8);
        graphView.getGraphViewStyle().setVerticalLabelsWidth(90);
        graphView.getGraphViewStyle().setNumHorizontalLabels(10);
        graphView.setViewPort(0, Xview);
        graphView.setScrollable(false);
        graphView.setScalable(true);
        graphView.setShowLegend(false);
        //graphView.setLegendAlign(LegendAlign.BOTTOM);
        graphView.setManualYAxis(true);
        graphView.setManualYAxisBounds(50, 0);
        graphView.addSeries(Series); // data
        GraphView.addView(graphView);
        graphView.getGraphViewStyle().setHorizontalLabelsColor(Color.BLACK);
        //graphView.getGraphViewStyle().setVerticalLabelsColor(Color.WHITE);
        graphView.getGraphViewStyle().setGridColor(Color.WHITE);
        graphView.setDrawBackground(true);
    }

    private File initLinkUser() {

        BufferedReader br;
        String temp;

        File session = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/open");

        try {
            if (session.exists()) {
                br = new BufferedReader(new FileReader(session));
                temp = br.readLine();
                emergency = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + temp + "/emergency_details.ram");
                session = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + temp + "/Thermal");

                if (!session.exists()) {

                    session.mkdirs();
                }

                timeNow = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());

                replayFile = new File(session, "Therm " + timeNow + ".replay");
                writeLog = new BufferedWriter(new FileWriter(replayFile, true));
                return session;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.thermo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_storage:
                if (sessionAccess == null) {
                    Toast.makeText(this, "No user logged in", Toast.LENGTH_SHORT).show();
                } else {
                    Intent replay = new Intent(ThermoActivity.this, ListReplayActivity.class);
                    replay.putExtra("userDir", sessionAccess);
                    replay.putExtra("heart", false);
                    Log.d("Calling him now", sessionAccess.toString());
                    startActivity(replay);
                    return true;
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mChatService == null) setupChat();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
    }

    @Override
    public synchronized void onDestroy() {
        super.onDestroy();

        sendBTMessage("t");

        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();

        if (sessionAccess != null) {
            try {
                Log.d("Writer", "Am I successful?");
                writeLog.write(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()) + "\n");
                writeLog.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setupChat() {

        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService = new BluetoothChatService(this, mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");

        // Get the device MAC address
        String address = getString(R.string.device_address);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device);
    }

    /**
     * Sends a message.
     */
    private void sendBTMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);

            // Reset out string buffer to zero
            mOutStringBuffer.setLength(0);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, "Error on enable Bluetooth", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

}
