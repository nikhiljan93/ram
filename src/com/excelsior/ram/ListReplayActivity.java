package com.excelsior.ram;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;


public class ListReplayActivity extends Activity {


    //GraphView_ecg init
    static LinearLayout GraphView_ecg;
    static GraphView graphView_ecg;
    static GraphViewSeries Series_ecg;
    //GraphView_beat init
    static LinearLayout GraphView_beat;
    static GraphView graphView_beat;
    static GraphViewSeries Series_beat;
    //GraphView_therm init
    static LinearLayout GraphView_therm;
    static LineGraphView graphView_therm;
    static GraphViewSeries Series_therm;
    //graph value
    private static double graph2LastXValue_ecg = 0;
    //graph value
    private static double graph2LastXValue_beat = 0;
    //graph value
    private static double graph2LastXValue_therm = 0;
    private static int Xview = 10;
    CalendarView cal;
    Button pickDate = null;
    File user;
    int indexLoc = 0;
    int delPos = 0;
    TextView tView = null;
    String replays_list[];
    boolean isEcg;
    ArrayList<String> curated_list = new ArrayList<String>();
    ArrayList<Integer> curated_index = new ArrayList<Integer>();

    private int yearGlobe;
    private int monthGlobe;
    private int dayGlobe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("History Viewer");
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_replay);

        Intent intent = getIntent();
        user = (File) intent.getSerializableExtra("userDir");
        isEcg = intent.getBooleanExtra("heart",false);

        cal = (CalendarView) findViewById(R.id.replay_date);
        pickDate = (Button) findViewById(R.id.replay_date_select);

        cal.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                yearGlobe = year;
                monthGlobe = month + 1;
                dayGlobe = dayOfMonth;
            }
        });

        pickDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Selected Date is\n\n"
                                + dayGlobe + " - " + monthGlobe + " - " + yearGlobe,
                        Toast.LENGTH_LONG
                ).show();

                processSelection();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
    }

    @Override
    public synchronized void onDestroy() {
        super.onDestroy();
    }

    private void processSelection() {

        setContentView(R.layout.activity_replay_files);

        final Button replay_next;
        final Button replay_prev;
        final Button replay_delete;
        Button replay_final;

        TextView replays_found;
        final TextView replay_fileview;

        replay_next = (Button) findViewById(R.id.replay_next);
        replay_prev = (Button) findViewById(R.id.replay_prev);
        replay_delete = (Button) findViewById(R.id.replay_delete);
        replay_final = (Button) findViewById(R.id.replay_final);

        replays_found = (TextView) findViewById(R.id.replays_found);
        replay_fileview = (TextView) findViewById(R.id.replay_fileview);

        generateReplays();

        if (curated_list.size() < 2) {
            replay_next.setEnabled(false);
            replay_prev.setEnabled(false);
            replay_delete.setEnabled(false);
            replay_final.setEnabled(false);
            replay_fileview.setText("No Records Found");
            return;
        }
        else if (curated_list.size()==2) {

            replay_next.setEnabled(false);
            replay_prev.setEnabled(false);
            replay_delete.setEnabled(true);
            replay_final.setEnabled(true);
            replay_fileview.setText(curated_list.get(indexLoc));
            replays_found.setText("1");
            Toast.makeText(ListReplayActivity.this, "You have reached the latest record.", Toast.LENGTH_LONG).show();

        }
        else   {
                replay_fileview.setText(curated_list.get(0));
                replay_prev.setEnabled(false);
                replay_next.setEnabled(true);
            replay_delete.setEnabled(true);
                replay_final.setEnabled(true);
                replays_found.setText((curated_list.size() - 1) + "");
            }

        replay_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indexLoc++;
                replay_next.setEnabled(true);
                replay_prev.setEnabled(true);
                replay_fileview.setText(curated_list.get(indexLoc));
                if (indexLoc >= curated_list.size() - 2) {
                    replay_next.setEnabled(false);
                    Toast.makeText(ListReplayActivity.this, "You have reached the latest record.", Toast.LENGTH_LONG).show();
                }
            }
        });

        replay_prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                indexLoc--;
                replay_prev.setEnabled(true);
                replay_next.setEnabled(true);
                replay_fileview.setText(curated_list.get(indexLoc));
                if (indexLoc <= 0) {
                    replay_prev.setEnabled(false);
                    Toast.makeText(ListReplayActivity.this, "You have reached the earliest record.", Toast.LENGTH_LONG).show();
                }
            }
        });

        replay_delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                File toDel = new File(user, replays_list[indexLoc]);
                toDel.delete();
                delPos = indexLoc;
            }
        });

        replay_final.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                startReplay(curated_index.get(indexLoc));

            }
        });

    }

    private void generateReplays() {

        replays_list = user.list();
        Arrays.sort(replays_list);

        if(isEcg) {

            /*for (int g = 0; g < replays_list.length; g++) {

                Log.d("This is sorted", replays_list[g]);
            } */     int keepCount = 0;

            for (int i = (replays_list.length / 2); i < replays_list.length; i++) {
                if(replays_list[i].substring(0,replays_list[i].indexOf(' ')).equalsIgnoreCase("Beat"))
                {
                    Log.d("Found Beat"," Haha");
                    continue;
                }
                if (verifyDate(replays_list[i])) {
                    curated_list.add(replays_list[i].substring((replays_list[i].indexOf(':')) - 2, replays_list[i].lastIndexOf('.')).trim());
                    curated_index.add(i);
                    Log.d("Curator ", curated_list.get(keepCount));
                    Log.d("Curator index",""+curated_list.get(keepCount));
                   // Log.d("Pure ", replays_list[i]);
                    keepCount++;
                }
            }
        }
        else    {

            for (int g = 0; g < replays_list.length; g++) {

                Log.d("This is sorted", replays_list[g]);
            }

            int keepCount = 0;

            for (int i = 0; i < replays_list.length; i++) {
                if (verifyDate(replays_list[i])) {
                    curated_list.add(replays_list[i].substring((replays_list[i].indexOf(':')) - 2, replays_list[i].lastIndexOf('.')).trim());
                    curated_index.add(i);
                    Log.d("Curator ", curated_list.get(keepCount));
                    Log.d("Curator index", ""+curated_index.get(keepCount));
                   // Log.d("Pure ", replays_list[i]);
                    keepCount++;
                }
            }
        }
    }

    private boolean verifyDate(String dateSel) {

        String checkDate;

        int tStamp[] = new int[3];

        int i = 0;

        checkDate = dateSel.substring((dateSel.indexOf('-')) - 2, (dateSel.indexOf(':')) - 3);
        checkDate.trim();

        StringTokenizer st = new StringTokenizer(checkDate, "-");

        while (st.hasMoreElements()) {
            tStamp[i] = Integer.parseInt(st.nextElement().toString());
            i++;
        }

        Log.d("Actual date ",dayGlobe+""+monthGlobe+""+yearGlobe+"");
        Log.d("File date ",tStamp[0]+""+tStamp[1]+""+tStamp[2]+"");

        if (tStamp[0] == dayGlobe && tStamp[1] == monthGlobe && tStamp[2] == yearGlobe) {
            return true;
        } else {
            return false;
        }

    }

    private void startReplay(int recordSelect) {

        File sessionAccess_ecg = null;
        //File sessionAccess_beat = null;
        File sessionAccess_therm;

        final Button replay_more;

        if(isEcg) {

            setContentView(R.layout.activity_replay_ecg);

            replay_more = (Button) findViewById(R.id.replay_ecg_new);

        /*------------Setting up ECG ------------*/

            //init graphview
            GraphView_ecg = (LinearLayout) findViewById(R.id.replay_Graph_ecg);
            // init example series data-------------------
            Series_ecg = new GraphViewSeries("",
                    new GraphViewSeries.GraphViewSeriesStyle(Color.rgb(40, 122, 169), 10),//color and thickness of the line
                    new GraphView.GraphViewData[]{new GraphView.GraphViewData(0, 0)});
            graphView_ecg = new LineGraphView(
                    this // context
                    , "" // heading
            );
            graphView_ecg.setViewPort(0, Xview);
            graphView_ecg.setScrollable(true);
            graphView_ecg.setScalable(false);
            graphView_ecg.setShowLegend(false);
            graphView_ecg.setLegendAlign(GraphView.LegendAlign.BOTTOM);
            graphView_ecg.setManualYAxis(true);
            graphView_ecg.setManualYAxisBounds(100, 0);
            graphView_ecg.addSeries(Series_ecg); // data
            GraphView_ecg.addView(graphView_ecg);
            graphView_ecg.getGraphViewStyle().setHorizontalLabelsColor(Color.TRANSPARENT);
            graphView_ecg.getGraphViewStyle().setVerticalLabelsColor(Color.TRANSPARENT);
            graphView_ecg.getGraphViewStyle().setGridColor(Color.BLACK);
            //graphView_ecg.setDrawingCacheEnabled(true);
            graphView_ecg.setClipToPadding(true);

        /*------------Setting up BPM ------------

            //init graphview
            GraphView_beat = (LinearLayout) findViewById(R.id.replay_Graph_beat);
            // init example series data-------------------
            Series_beat = new GraphViewSeries("",
                    new GraphViewSeries.GraphViewSeriesStyle(Color.rgb(255, 0, 0), 10),//color and thickness of the line
                    new GraphView.GraphViewData[]{new GraphView.GraphViewData(0, 0)});
            graphView_beat = new LineGraphView(
                    this // context
                    , "" // heading
            );
            graphView_beat.setViewPort(0, Xview);
            graphView_beat.setScrollable(true);
            graphView_beat.setScalable(false);
            graphView_beat.setShowLegend(false);
            graphView_beat.setLegendAlign(GraphView.LegendAlign.BOTTOM);
            graphView_beat.setManualYAxis(true);
            graphView_beat.setManualYAxisBounds(200, 0);
            graphView_beat.addSeries(Series_beat); // data
            GraphView_beat.addView(graphView_beat);
            graphView_beat.getGraphViewStyle().setHorizontalLabelsColor(Color.TRANSPARENT);
            graphView_beat.getGraphViewStyle().setVerticalLabelsColor(Color.TRANSPARENT);
            graphView_beat.getGraphViewStyle().setGridColor(Color.BLACK);
            //graphView_beat.setDrawingCacheEnabled(true);
            graphView_ecg.setClipToPadding(true);*/

            sessionAccess_ecg = new File(user, replays_list[recordSelect]);
            //sessionAccess_beat = new File(user, replays_list[recordSelect]);
        }
        else    {

            setContentView(R.layout.activity_replay_therm);

            tView = (TextView) findViewById(R.id.replay_temp);

            replay_more = (Button) findViewById(R.id.replay_therm_new);

            //init graphview
            GraphView_therm = (LinearLayout) findViewById(R.id.Graph);
            // init example series data-------------------
            Series_therm = new GraphViewSeries("",
                    new GraphViewSeries.GraphViewSeriesStyle(Color.TRANSPARENT, 10),//color and thickness of the line
                    new GraphView.GraphViewData[]{new GraphView.GraphViewData(0, 0)});
            graphView_therm = new LineGraphView(
                    this // context
                    , "" // heading
            );
            graphView_therm.setBackgroundColor(Color.argb(128, 40, 122, 169));
            graphView_therm.getGraphViewStyle().setNumVerticalLabels(8);
            graphView_therm.getGraphViewStyle().setNumHorizontalLabels(10);
            graphView_therm.getGraphViewStyle().setVerticalLabelsWidth(90);
            graphView_therm.setViewPort(0, Xview);
            graphView_therm.setScrollable(false);
            graphView_therm.setScalable(true);
            graphView_therm.setShowLegend(false);
            //graphView.setLegendAlign(LegendAlign.BOTTOM);
            graphView_therm.setManualYAxis(true);
            graphView_therm.setManualYAxisBounds(50, 0);
            graphView_therm.addSeries(Series_therm); // data
            GraphView_therm.addView(graphView_therm);
            graphView_therm.getGraphViewStyle().setHorizontalLabelsColor(Color.BLACK);
            //graphView.getGraphViewStyle().setVerticalLabelsColor(Color.WHITE);
            graphView_therm.getGraphViewStyle().setGridColor(Color.BLACK);
            graphView_therm.setDrawBackground(true);
        }

        try {

            if(isEcg) {

                final BufferedReader ecg = new BufferedReader(new FileReader(sessionAccess_ecg));
                //final BufferedReader beat = new BufferedReader(new FileReader(sessionAccess_beat));
                Log.d("File ECG Index ", (recordSelect + (replays_list.length / 2)) + "");
               // Log.d("File Beat Index ", recordSelect + "");
                Log.d("FileECG Val ", sessionAccess_ecg.getAbsolutePath());
               // Log.d("FileBeat Val ", sessionAccess_beat.getAbsolutePath());

                replay_more.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String temp1, temp2;
                        int filtered;
                        //double peaks_processed;

                            try {

                                for (int y = 0; y < 200; y++) {

                                    temp1 = ecg.readLine();

                                    if (temp1 == null) {
                                        Toast.makeText(ListReplayActivity.this, "You have reached the end of the log", Toast.LENGTH_LONG).show();
                                        replay_more.setEnabled(false);
                                        return;
                                    }

                                    Log.d("ECG Val ", temp1);
                                 //   Log.d("Beat Val ", temp2);

                                    filtered = Integer.parseInt(temp1);
                                 //   peaks_processed = Double.parseDouble(temp2);

                                    Series_ecg.appendData(new GraphView.GraphViewData(graph2LastXValue_ecg, filtered), true);
                                  //  Series_beat.appendData(new GraphView.GraphViewData(graph2LastXValue_beat, (int) peaks_processed), true);


                                    //X-axis control
                                    graph2LastXValue_ecg += 0.05;
                                //    graph2LastXValue_beat += 0.05;

                                    graphView_ecg.setViewPort(graph2LastXValue_ecg - Xview, Xview);
                               //     graphView_beat.setViewPort(graph2LastXValue_beat - Xview, Xview);

                                    //refresh
                                    GraphView_ecg.removeView(graphView_ecg);
                                    GraphView_ecg.addView(graphView_ecg);
                               //     GraphView_beat.removeView(graphView_beat);
                               //     GraphView_beat.addView(graphView_beat);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                    }

                });
            }
            else    {
                sessionAccess_therm = new File(user, replays_list[recordSelect]);
                final BufferedReader therm = new BufferedReader(new FileReader(sessionAccess_therm));

                Log.d("FileTherm Val ", sessionAccess_therm.getAbsolutePath());

                replay_more.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String temp1;
                        double filtered;

                        try {
                            temp1 = therm.readLine();

                            if (temp1 == null)  {
                                replay_more.setEnabled(false);
                                Toast.makeText(ListReplayActivity.this, "You have reached the end of the log", Toast.LENGTH_LONG).show();
                                return;
                            }

                            Log.d("Therm Val ", temp1);

                            filtered = Double.parseDouble(temp1);
                            Series_therm.appendData(new GraphView.GraphViewData(graph2LastXValue_therm, filtered), true);

                            //X-axis control
                            graph2LastXValue_therm += 0.750;

                            graphView_therm.setViewPort(graph2LastXValue_therm - Xview, Xview);

                            //refresh
                            GraphView_therm.removeView(graphView_therm);
                            GraphView_therm.addView(graphView_therm);
                            tView.setText(filtered + " " + (char) 0x00B0 + "C");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

