package com.excelsior.ram;

import android.os.AsyncTask;
import android.telephony.SmsManager;


public class SmsService extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(params[0], null, params[1], null, null);

        } catch (Exception e) {

        }

        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
        // might want to change "executed" for the returned string passed
        // into onPostExecute() but that is upto you
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}