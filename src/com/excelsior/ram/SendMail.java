package com.excelsior.ram;

import android.os.AsyncTask;

class SendMail extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
        Mail m = new Mail("health.bot.ram@gmail.com", "testPhase");
        try {

            m.sendMail("Emergency!", params[0], "health.bot.ram@gmail.com", params[1],params[2]);
        } catch (Exception e) {

        }
        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
        // might want to change "executed" for the returned string passed
        // into onPostExecute() but that is upto you
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}