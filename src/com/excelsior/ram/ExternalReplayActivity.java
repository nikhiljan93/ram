package com.excelsior.ram;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class ExternalReplayActivity extends Activity {

    //GraphView_ecg init
    static LinearLayout GraphView_ecg;
    static GraphView graphView_ecg;
    static GraphViewSeries Series_ecg;
    //GraphView_therm init
    static LinearLayout GraphView_therm;
    static LineGraphView graphView_therm;
    static GraphViewSeries Series_therm;
    //graph value
    private static double graph2LastXValue_ecg = 0;
    //graph value
    private static double graph2LastXValue_therm = 0;
    private static int Xview = 10;
    String filePath;
    CalendarView cal;
    Button pickDate = null;
    File user;
    int indexLoc = 0;
    int delPos = 0;
    TextView tView = null;
    boolean isEcg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("History Viewer");
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //setContentView(R.layout.activity_replay);

        Intent intent = getIntent();

        filePath = intent.getData().getPath();
        Log.d("filePath", filePath + "");

        isEcg = checkType(intent.getData().getLastPathSegment());

        startReplay();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
    }

    @Override
    public synchronized void onDestroy() {
        super.onDestroy();
    }

    private boolean checkType(String temp) {

        String temp1;
        temp1 = temp.substring(0, temp.indexOf(' '));
        Log.d("checkingType", temp1 + " !");
        if (temp1.equalsIgnoreCase("ECG")) {
            Log.d("checkingType", "isECG");
            return true;
        } else {
            Log.d("checkingType", "isnotECG");
            return false;
        }
    }

    private void startReplay() {

        File sessionAccess_ecg = null;

        File sessionAccess_therm;

        final Button replay_more;

        if (isEcg) {

            setContentView(R.layout.activity_replay_ecg);

            replay_more = (Button) findViewById(R.id.replay_ecg_new);

        /*------------Setting up ECG ------------*/

            //init graphview
            GraphView_ecg = (LinearLayout) findViewById(R.id.replay_Graph_ecg);
            // init example series data-------------------
            Series_ecg = new GraphViewSeries("",
                    new GraphViewSeries.GraphViewSeriesStyle(Color.rgb(40, 122, 169), 10),//color and thickness of the line
                    new GraphView.GraphViewData[]{new GraphView.GraphViewData(0, 0)});
            graphView_ecg = new LineGraphView(
                    this // context
                    , "" // heading
            );
            graphView_ecg.setViewPort(0, Xview);
            graphView_ecg.setScrollable(true);
            graphView_ecg.setScalable(false);
            graphView_ecg.setShowLegend(false);
            graphView_ecg.setLegendAlign(GraphView.LegendAlign.BOTTOM);
            graphView_ecg.setManualYAxis(true);
            graphView_ecg.setManualYAxisBounds(100, 0);
            graphView_ecg.addSeries(Series_ecg); // data
            GraphView_ecg.addView(graphView_ecg);
            graphView_ecg.getGraphViewStyle().setHorizontalLabelsColor(Color.TRANSPARENT);
            graphView_ecg.getGraphViewStyle().setVerticalLabelsColor(Color.TRANSPARENT);
            graphView_ecg.getGraphViewStyle().setGridColor(Color.BLACK);
            //graphView_ecg.setDrawingCacheEnabled(true);
            graphView_ecg.setClipToPadding(true);

            sessionAccess_ecg = new File(filePath);
        } else {

            setContentView(R.layout.activity_replay_therm);

            tView = (TextView) findViewById(R.id.replay_temp);

            replay_more = (Button) findViewById(R.id.replay_therm_new);

            //init graphview
            GraphView_therm = (LinearLayout) findViewById(R.id.Graph);
            // init example series data-------------------
            Series_therm = new GraphViewSeries("",
                    new GraphViewSeries.GraphViewSeriesStyle(Color.TRANSPARENT, 10),//color and thickness of the line
                    new GraphView.GraphViewData[]{new GraphView.GraphViewData(0, 0)});
            graphView_therm = new LineGraphView(
                    this // context
                    , "" // heading
            );
            graphView_therm.setBackgroundColor(Color.argb(128, 40, 122, 169));
            graphView_therm.getGraphViewStyle().setNumVerticalLabels(8);
            graphView_therm.getGraphViewStyle().setNumHorizontalLabels(10);
            graphView_therm.getGraphViewStyle().setVerticalLabelsWidth(90);
            graphView_therm.setViewPort(0, Xview);
            graphView_therm.setScrollable(false);
            graphView_therm.setScalable(true);
            graphView_therm.setShowLegend(false);
            //graphView.setLegendAlign(LegendAlign.BOTTOM);
            graphView_therm.setManualYAxis(true);
            graphView_therm.setManualYAxisBounds(50, 0);
            graphView_therm.addSeries(Series_therm); // data
            GraphView_therm.addView(graphView_therm);
            graphView_therm.getGraphViewStyle().setHorizontalLabelsColor(Color.BLACK);
            //graphView.getGraphViewStyle().setVerticalLabelsColor(Color.WHITE);
            graphView_therm.getGraphViewStyle().setGridColor(Color.BLACK);
            graphView_therm.setDrawBackground(true);
        }

        try {

            if (isEcg) {

                final BufferedReader ecg = new BufferedReader(new FileReader(sessionAccess_ecg));
                replay_more.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String temp1, temp2;
                        int filtered;
                        //double peaks_processed;

                        try {

                            for (int y = 0; y < 200; y++) {

                                temp1 = ecg.readLine();

                                if (temp1 == null) {
                                    Toast.makeText(ExternalReplayActivity.this, "You have reached the end of the log", Toast.LENGTH_LONG).show();
                                    replay_more.setEnabled(false);
                                    return;
                                }

                                Log.d("ECG Val ", temp1);

                                filtered = Integer.parseInt(temp1);

                                Series_ecg.appendData(new GraphView.GraphViewData(graph2LastXValue_ecg, filtered), true);

                                //X-axis control
                                graph2LastXValue_ecg += 0.05;

                                graphView_ecg.setViewPort(graph2LastXValue_ecg - Xview, Xview);

                                //refresh
                                GraphView_ecg.removeView(graphView_ecg);
                                GraphView_ecg.addView(graphView_ecg);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
            } else {
                sessionAccess_therm = new File(filePath);
                final BufferedReader therm = new BufferedReader(new FileReader(sessionAccess_therm));

                Log.d("FileTherm Val ", sessionAccess_therm.getAbsolutePath());

                replay_more.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        String temp1;
                        double filtered;

                        try {
                            temp1 = therm.readLine();

                            if (temp1 == null) {
                                replay_more.setEnabled(false);
                                Toast.makeText(ExternalReplayActivity.this, "You have reached the end of the log", Toast.LENGTH_LONG).show();
                                return;
                            }

                            Log.d("Therm Val ", temp1);

                            filtered = Double.parseDouble(temp1);
                            Series_therm.appendData(new GraphView.GraphViewData(graph2LastXValue_therm, filtered), true);

                            //X-axis control
                            graph2LastXValue_therm += 0.750;

                            graphView_therm.setViewPort(graph2LastXValue_therm - Xview, Xview);

                            //refresh
                            GraphView_therm.removeView(graphView_therm);
                            GraphView_therm.addView(graphView_therm);
                            tView.setText(filtered + " " + (char) 0x00B0 + "C");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

