package com.excelsior.ram;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class UserManagement extends Activity {

    String user;
    EditText mEdit;

    Button button_add = null;
    Button button_del = null;
    Button button_login = null;
    Button open_cc = null;

    TextView current_login = null;

    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        setContentView(R.layout.activity_user_management);

        open_cc = (Button) findViewById(R.id.button_open_cc);
        open_cc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent fa = new Intent(UserManagement.this, UserLoginActivity.class);
                fa.putExtra("id", user);
                startActivity(fa);
            }
        });

        button_add = (Button) findViewById(R.id.button_add_user);
        button_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addUser();
            }
        });

        button_del = (Button) findViewById(R.id.button_delete);
        button_del.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                delUser();
            }
        });

        button_login = (Button) findViewById(R.id.button_login);
        button_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!checkForOpenSession())
                    startSession();

                else {
                    Log.d("SessionPolice", "I got here as fast as I could");
                    stopSession();
                }
            }
        });

        current_login = (TextView) findViewById(R.id.show_current_login);

        if (checkForOpenSession()) {
            open_cc.setEnabled(true);
            button_login.setText("Logout");
            Toast.makeText(this, "Session already open", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_management, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addUser() {

        //Initialise Folder as per the user's name
        mEdit = (EditText) findViewById(R.id.new_user);
        user = mEdit.getText().toString();

        File userFolder = null;
        id = new Random().nextInt(1000) + 1;

        try {
            userFolder = createNewUser(user, id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent e = new Intent(UserManagement.this, AddUserActivity.class);
        e.putExtra("user", userFolder);
        e.putExtra("id", id);
        startActivity(e);
    }

    private void delUser() {

        BufferedReader temp;

        mEdit = (EditText) findViewById(R.id.existingID);
        user = mEdit.getText().toString();

        if (user.equalsIgnoreCase("69")) {
            Toast.makeText(this, "Haha, nice!", Toast.LENGTH_SHORT).show();
        }

        File digIn = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + user);

        try {
            if (digIn.exists()) {
                if (checkForOpenSession()) {
                    temp = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/open")));
                    if (temp.readLine().equalsIgnoreCase(user)) {
                        Toast.makeText(this, "User currently logged in. Cannot delete", Toast.LENGTH_LONG).show();
                        temp.close();
                        return;
                    }
                }
                delete(digIn);
                Toast.makeText(this, "User removed", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "User does not exist. Check your ID", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private File createNewUser(String userName, int randomNum) throws IOException {

        File temp = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + randomNum);
        temp.mkdirs();

        File temp1 = new File(temp, getString(R.string.file_id));

        BufferedWriter bw = new BufferedWriter(new FileWriter(temp1));

        bw.write(userName + "\n");
        bw.write(randomNum + "");
        bw.close();

        return temp;
    }

    private void startSession() {

        mEdit = (EditText) findViewById(R.id.existingID);
        user = mEdit.getText().toString();

        if (user.equalsIgnoreCase("")) {

            Toast.makeText(this, "This field cannot be blank!", Toast.LENGTH_LONG).show();
            return;
        }

        BufferedWriter temp;

        File digIn = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/" + user);

        try {
            if (digIn.exists()) {

                current_login.setText(user);
                button_login.setText("Logout");
                digIn = new File(new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/"), "open");

                temp = new BufferedWriter(new FileWriter(digIn));

                temp.write(user + "");
                temp.close();
                //NavUtils.navigateUpFromSameTask(this);
                open_cc.setEnabled(true);
                Toast.makeText(this, "User logged in", Toast.LENGTH_SHORT).show();
                Intent f = new Intent(UserManagement.this, UserLoginActivity.class);
                f.putExtra("id", user);
                startActivity(f);
            } else
                Toast.makeText(this, "User does not exist. Check your ID", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopSession() {

        mEdit = (EditText) findViewById(R.id.existingID);
        user = mEdit.getText().toString();

        File digIn = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/open");

        try {
            if (digIn.exists()) {
                current_login.setText(null);
                button_login.setText("Login");
                digIn.delete();
                open_cc.setEnabled(false);
                //NavUtils.navigateUpFromSameTask(this);
                Toast.makeText(this, "User logged out", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkForOpenSession() {

        BufferedReader temp;

        File digIn = new File(Environment.getExternalStorageDirectory().getPath() + "/RAM/profiles/open");

        try {
            if (digIn.exists()) {

                temp = new BufferedReader(new FileReader(digIn));
                user = temp.readLine();
                current_login.setText(user);
                temp.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles())
                delete(c);
        }
        if (!f.delete())
            throw new FileNotFoundException("Failed to delete file: " + f);
    }

}
